#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/superres/optical_flow.hpp>
#include <HalconCpp.h>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include "common.h"

using namespace cv;
using namespace cv::superres;
using namespace std;
using namespace HalconCpp;

void cv_OpticalFlow(string algorithm = "farneback") {
    Mat srcImg;
    srcImg = imread("image/sample.jpg");
    // printf("%d", srcImg.size().width);

    ofstream outputfile("csv/cv_optflow_" + algorithm + ".csv");

    for (int h = 50; h <= HEIGHT_MAX; h++) {
        cout << "h:" << h << endl;
    
        Mat beforeImg, beforeGray;
        Mat afterImg, afterGray;
        
        beforeImg = srcImg(Rect(0,0,h,h)).clone();
        Size imgSize = beforeImg.size();

        Mat matrix = getRotationMatrix2D(Point2f(imgSize.width / 2, imgSize.height / 2), 5.0, 1.0);
        warpAffine(beforeImg, afterImg, matrix, Size(imgSize.width, imgSize.height));

        cvtColor(beforeImg, beforeGray, COLOR_RGB2GRAY);
        cvtColor(afterImg, afterGray, COLOR_RGB2GRAY);

        vector<double> time;
        Mat newFrame;
        
        for (int i = 0; i < REPEAT_COUNT; i++) {
            double s = HSystem::CountSeconds();

            Mat flow;
            if (algorithm == "farneback") {
                calcOpticalFlowFarneback(beforeGray, afterGray, flow, 0.8, 10, 15, 3, 5, 1.1,0);
            }
            else if (algorithm == "dual_tvl1") {
                Ptr<DenseOpticalFlowExt> opticalFlow = superres::createOptFlow_DualTVL1();
                opticalFlow->calc(beforeGray, afterGray, flow);
            }

            Mat map(flow.size(), CV_32FC2);
            for (int y = 0; y < map.rows; ++y) {
                for (int x = 0; x < map.cols; ++x) {
                    Point2f f = flow.at<Point2f>(y, x);
                    map.at<Point2f>(y, x) = Point2f(x + f.x, y + f.y);
                }
            }

            remap(afterGray, newFrame, map, Mat(), INTER_LINEAR);

            double e = HSystem::CountSeconds();
            time.push_back(e - s);
        }
        
        sort(begin(time), end(time));
        size_t median_index = size(time) / 2;
        double median = (size(time) % 2 == 0
            ? static_cast<double>(time[median_index] + time[median_index - 1]) / 2
            : time[median_index]);
		outputfile << median << endl;

        if (h > HEIGHT_MAX - 5) {
			string writePath = "image/cv_optflow_" + algorithm + "_" + std::to_string(h) + ".jpg";
            imwrite(writePath, newFrame);
		}
    }

    outputfile.close();
}

void cv_OpticalFlow_ocl(string algorithm = "farneback") {
    Mat srcImg;
    srcImg = imread("image/sample.jpg");
    // printf("%d", srcImg.size().width);
    UMat u_srcImg = srcImg.getUMat(ACCESS_READ);

    ofstream outputfile("csv/cv_optflow_ocl_" + algorithm + ".csv");

    for (int h = 50; h <= HEIGHT_MAX; h++) {
        cout << "h:" << h << endl;
        
        UMat beforeImg, beforeGray;
        UMat afterImg, afterGray;
        
        beforeImg = u_srcImg(Rect(0,0,h,h)).clone();
        Size imgSize = beforeImg.size();

        Mat matrix = getRotationMatrix2D(Point2f(imgSize.width / 2, imgSize.height / 2), 5.0, 1.0);
        warpAffine(beforeImg, afterImg, matrix, Size(imgSize.width, imgSize.height));

        cvtColor(beforeImg, beforeGray, COLOR_RGB2GRAY);
        cvtColor(afterImg, afterGray, COLOR_RGB2GRAY);

        vector<double> time;
        UMat newFrame;

        for (int i = 0; i < REPEAT_COUNT; i++) {
            double s = HSystem::CountSeconds();

            UMat flow;
            if (algorithm == "farneback") {
                calcOpticalFlowFarneback(beforeGray, afterGray, flow, 0.8, 10, 15, 3, 5, 1.1,0);
            }
            else if (algorithm == "dual_tvl1") {
                Ptr<DenseOpticalFlowExt> opticalFlow = superres::createOptFlow_DualTVL1();
                opticalFlow->calc(beforeGray, afterGray, flow);
            }

            UMat map(flow.size(), CV_32FC2);
            for (int y = 0; y < map.rows; ++y) {
                for (int x = 0; x < map.cols; ++x) {
                    Point2f f = flow.getMat(ACCESS_READ).at<Point2f>(y, x);
                    map.getMat(ACCESS_READ).at<Point2f>(y, x) = Point2f(x + f.x, y + f.y);
                }
            }

            remap(afterGray, newFrame, map, UMat(), INTER_LINEAR);

            double e = HSystem::CountSeconds();
            time.push_back(e - s);
        }

        sort(begin(time), end(time));
        size_t median_index = size(time) / 2;
        double median = (size(time) % 2 == 0
            ? static_cast<double>(time[median_index] + time[median_index - 1]) / 2
            : time[median_index]);
		outputfile << median << endl;

        if (h > HEIGHT_MAX - 5) {
			string writePath = "image/cv_optflow_ocl_" + algorithm + "_" + std::to_string(h) + ".jpg";
            imwrite(writePath, newFrame);
		}
    }

    outputfile.close();
}