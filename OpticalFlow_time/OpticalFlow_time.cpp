﻿#include <iostream>
#include "cv_optflow.h"
#include "halcon_optflow.h"

using namespace std;

int main() {
	cout << "OpenCV Optical Flow Farneback" << endl;
	cv_OpticalFlow("farneback");
	cout << "OpenCV Optical Flow Dual TV-L1" << endl;
	cv_OpticalFlow("dual_tvl1");
	cout << "OpenCV Optical Flow Farneback with OpenCL" << endl;
	cv_OpticalFlow_ocl("farneback");
	cout << "OpenCV Optical Flow Dual TV-L1 with OpenCL" << endl;
	cv_OpticalFlow_ocl("dual_tvl1");
	cout << "HALCON Optical Flow clg" << endl;
	h_OpticalFlow("clg");
	cout << "HALCON Optical Flow ddraw" << endl;
	h_OpticalFlow("ddraw");
	cout << "HALCON Optical Flow fdrig" << endl;
	h_OpticalFlow("fdrig");
	cout << "HALCON Optical Flow clg with OpenCL" << endl;
	h_OpticalFlow_ocl("clg");
	cout << "HALCON Optical Flow ddraw with OpenCL" << endl;
	h_OpticalFlow_ocl("ddraw");
	cout << "HALCON Optical Flow fdrig with OpenCL" << endl;
	h_OpticalFlow_ocl("fdrig");
}