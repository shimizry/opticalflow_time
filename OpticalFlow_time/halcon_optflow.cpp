#include "HalconCpp.h"
#include "common.h"
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;
using namespace HalconCpp;

void h_OpticalFlow(string algorithm = "fdrig", bool ocl = false) {
    HObject srcImg;
	ReadImage(&srcImg, "image/sample.jpg");
	//HTuple width; HTuple height;
	//GetImageSize(srcImg, &width, &height);
	//cout << "size:" << width.I() << endl;
    
    string csvPath;
    if (ocl)
        csvPath = "csv/h_optflow_ocl_" + algorithm + ".csv";
    else
        csvPath = "csv/h_optflow_" + algorithm + ".csv";

    ofstream outputfile(csvPath);
    
	
    for (int h = 50; h <= HEIGHT_MAX; h++) {
        cout << "h:" << h << endl;

        HObject beforeImg, beforeGray; 
        HObject afterImg, afterGray;
        CropRectangle1(srcImg, &beforeImg, 0, 0, h - 1, h - 1);
        RotateImage(beforeImg, &afterImg, HTuple(-5.0),"nearest_neighbor");
        Rgb1ToGray(beforeImg, &beforeGray);
        Rgb1ToGray(afterImg, &afterGray);

        vector<double> time;
        HObject newFrame;

        for (int i = 0; i < REPEAT_COUNT; i++) {
            double s = HSystem::CountSeconds();

            HObject flow;
            OpticalFlowMg(beforeGray, afterGray, &flow, HTuple(algorithm.c_str()), 2, 1, 30, 5, "default_parameters", "fast");
            UnwarpImageVectorField(afterGray, flow, &newFrame);

            double e = HSystem::CountSeconds();
            time.push_back(e - s);
        }
        sort(begin(time), end(time));
        size_t median_index = size(time) / 2;
        double median = (size(time) % 2 == 0
            ? static_cast<double>(time[median_index] + time[median_index - 1]) / 2
            : time[median_index]);
		outputfile << median << endl;

        if (h > HEIGHT_MAX - 5) {
            string writePath;
            if (ocl)
			    writePath = "image/h_optflow_ocl_" + algorithm + "_" + std::to_string(h);
            else
                writePath = "image/h_optflow_" + algorithm + "_" + std::to_string(h);
            WriteImage(newFrame, "jpg", 0, HTuple(writePath.c_str()));
		}
    }

	outputfile.close();
}

void h_OpticalFlow_ocl(string algorithm = "fdrig") {
	HTuple deviceIdentifier, info, deviceHandle;
	QueryAvailableComputeDevices(&deviceIdentifier);
	GetComputeDeviceInfo(deviceIdentifier[0], "name", &info);
	OpenComputeDevice(0, &deviceHandle);
	ActivateComputeDevice(deviceHandle);
	InitComputeDevice(deviceHandle, "optical_flow_mg");
	InitComputeDevice(deviceHandle, "unwarp_image_vector_field");
	h_OpticalFlow(algorithm, true);
}